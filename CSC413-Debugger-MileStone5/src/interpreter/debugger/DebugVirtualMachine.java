package interpreter.debugger;

import interpreter.ByteCodeSyntaxErrorException;
import interpreter.Program;
import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

/**
 * DebugVirtualMachine is a child of VirtualMachine. It is run by the
 * UserInterface to allow for higher level processing of uncompiled .x source
 * code
 *
 * @author frank
 */
public class DebugVirtualMachine extends VirtualMachine {

    //Vector of our sourcelines and breakpoints
    private Vector<Entry> sourceCodeLines; //A vector of Entry's, one for each line of sourceCode
    private Stack<FunctionEnvironmentRecord> environmentStack; //A stack to hold FunctionEnvironmentRecord

    private Set<Integer> validBreakpointLines; //A set that contains the lines that are valid to break on
    private FunctionEnvironmentRecord currRecord; //Reference to the current record.

    private BufferedReader source; //BufferedReader for our source file

    private boolean debugJustEnteredFunction = false;
    private boolean programTerminated = false;

    //Debug flags to enable StepOut/StepIn/StepOver/Trace
    private boolean stepOutSet = false;
    private boolean stepInSet = false;
    private boolean stepOverSet = false;
    private boolean traceSet = false;

    //Debug flag whether we are asking to break
    private boolean askToBreak = false;

    //To enable Step/Trace we need to keep track of the current Activation record size
    //and the line we started and on, depending on the function we watch for these values
    //to change
    private int debugActivationRecordSize = 0;
    private int debugWatchLine = 0;

    /**
     * Default constructor, takes a Program object from the DebugByteCodeLoader
     * A String that points to the sourceCodeFileName which will be opened and
     * processed and a Set containing all the valid lines to set breakpoints at.
     *
     * @param program
     * @param sourceCodeFileName
     * @param validBreakPoints
     */
    public DebugVirtualMachine(Program program, String sourceCodeFileName, Set<Integer> validBreakPoints) {
        super(program); //Call base VirtualMachine
        try
        {
            //Initialize sourceCodeLines and environmentStack
            sourceCodeLines = new Vector<Entry>();
            environmentStack = new Stack<FunctionEnvironmentRecord>();

            //Setup validBreakPoints
            this.validBreakpointLines = validBreakPoints;

            //Open the sourceFile
            source = new BufferedReader(new FileReader(sourceCodeFileName));

            String currString;

            do
            {
                //Read the next line
                currString = source.readLine();

                if (currString != null)
                {
                    Entry currEntry = new Entry(currString, false);

                    sourceCodeLines.add(currEntry); //Add line to sourceCodeLines
                }

            } while (currString != null);

            //Create an initial FunctionEnvironmentRecord
            FunctionEnvironmentRecord firstRecord = new FunctionEnvironmentRecord();
            firstRecord.setStartLine(1);
            firstRecord.setEndLine(sourceCodeLines.size());
            environmentStack.push(firstRecord);

        } catch (FileNotFoundException ex)
        {
            System.out.println("***IOException cannot find file: " + sourceCodeFileName);
            System.exit(1);
        } catch (IOException ex)
        {
            System.out.println("***IOException can't read from file: " + sourceCodeFileName);
            System.exit(1);
        }

    }

    /**
     * UserInterface calls this when a user continues execution. This then calls
     * the underlying VirtualMachine. This is done to prevent the user from
     * continuing once a program has halted.
     */
    public boolean debugExecuteProgram() {
        if (!programTerminated)
        {
            //Make sure the program has not already terminated
            //I.E. its ran through and hit HALT already
            this.executeProgram();
            return true;
        }
        else
        {
            System.out.println("Program has already finished execution!");
            return false;
        }
    }

    /**
     * Create a new environment record and push it onto the environmentStack
     */
    public void createNewEnvironmentRecord() {
        //Create a new EnvionrmentRecord now
        FunctionEnvironmentRecord currEnv = new FunctionEnvironmentRecord();
        environmentStack.push(currEnv);
    }

    /**
     * Used keep track of if we have just entered a function
     */
    public void setJustEnteredFunction() {
        this.debugJustEnteredFunction = true;
    }

    /**
     * UserInterface calls this when execution is halted, this is to prevent a
     * user from continuing execution after a program has halted.
     */
    @Override
    public void haltExecution() {

        super.haltExecution();

        //Set programTerminated to true and then call super function
        programTerminated = true;

        //Set current execution line to the last one in the functionEnvironmentRecord
        FunctionEnvironmentRecord currRecord = this.getCurrentEnvironmentRecord();

        //Makes -> goto end of source code for prettier printing
        currRecord.setCurrentLine(currRecord.getStopLine());

        //System.out.println("******Execution Halted*******");
    }

    /**
     * Remove justEnteredFunction flag
     */
    public void unsetJustEnteredFunction() {
        this.debugJustEnteredFunction = false;
    }

    /**
     * Set a functions name, startline and endline in the source code.
     *
     * @param funcName
     * @param startLine
     * @param stopLine
     */
    public void setFunction(String funcName, int startLine, int stopLine) {
        if (!environmentStack.empty())
        {
            environmentStack.peek().setFunction(funcName, startLine, stopLine);

        }

        unsetJustEnteredFunction(); //Unset the flag since we have processed the current function.

        if (!runStack.isEmpty() && traceSet)
        {
            printTraceFunctionEntry(funcName, runStack.peek());
        }

    }

    /**
     * Retrieve the current environment record
     *
     * @return
     */
    public FunctionEnvironmentRecord getCurrentEnvironmentRecord() {
        return this.environmentStack.peek();
    }

    /**
     * Set the current execution line in the current environment record
     *
     * @param currentLine
     */
    public void setCurrentLine(int currentLine) {
        if (!environmentStack.empty())
        {
            environmentStack.peek().setCurrentLine(currentLine);
        }

        //Check to see if new line has a breakpoint at it.
        if (!(currentLine < 1))
        {
            if (sourceCodeLines.get(currentLine - 1).isBreakptSet)
            {
                //Tell underlying VM to halt
                this.requestBreak();
            }
        }

    }

    /**
     * Request that the VM break
     */
    private void requestBreak() {
        this.askToBreak = true;
    }

    /**
     * pop n symbols from environment stack
     *
     * @param n
     */
    public void popSymbolTable(int n) {
        //Pop the symbols for environmentStack
        if (!environmentStack.empty())
        {
            environmentStack.peek().pop(n);
        }
    }

    /**
     * Pop the top environment record
     */
    public void popEnvironmentRecord() {
        //Pop the top environmentRecord
        if (!environmentStack.empty())
        {
            environmentStack.pop();
        }

    }

    /**
     * Retrieve the current environment records source code
     *
     * @return
     */
    public Vector<Entry> getCurrentFunctionSource() {
        //Return the source code for the currently executing function
        //If there is no entry on environmentStack then we return the entire program

        Vector<Entry> currFunctionEntries;

        if (environmentStack.empty())
        {
            //No record started so return the entire program
            currFunctionEntries = sourceCodeLines;
        }
        else
        {
            //Stack is not empty so retrieve the top of environmentStack
            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            //Create a new vector
            currFunctionEntries = new Vector<Entry>();

            //Check to see if we are inside an intrinsicFunction
            if (currEnvRecord.getStartLine() == -1)
            {
                String intrinsicOutput = "****Intrinsic Function: ";
                intrinsicOutput += currEnvRecord.getFunctionName();
                Entry intrinsicEntry = new Entry(intrinsicOutput, false);

                currFunctionEntries.add(intrinsicEntry);

            }
            else
            {

                int startLine = 0;
                int stopLine = 0;

                //Get the start and stop lines.
                startLine = currEnvRecord.getStartLine();
                stopLine = currEnvRecord.getStopLine();

                for (int i = (startLine - 1); i < stopLine; i++)
                {
                    currFunctionEntries.add(sourceCodeLines.get(i));
                }
            }
        }

        //Return the reference to our sourceCode
        return currFunctionEntries;

    }

    @Override
    /**
     * Tell VM to begin executing Program
     */
    public void executeProgram() {
        try
        {
            //Acquire the first line of the program
            currBC = program.requestByteCodeAtAddr(pc);
            isRunning = true; //Begin program execution, HALT must be hit for this to be set false
            while (isRunning)
            {

                //Code for StepOut, StepIn, StepOver
                if (stepOutSet)
                {
                    if (debugActivationRecordSize > environmentStack.size())
                    {
                        //Check to see if we are outside the current activationRecord
                        //if so break
                        stepOutSet = false;
                        askToBreak = true;
                    }
                }
                else if (stepInSet)
                {
                    //If we are not currently on a function then step in just steps over the line
                    //otherwise we check to see if we have added an activationRecord to the environmentStack
                    //If either condition is true, then break.
                    if ((debugActivationRecordSize < environmentStack.size())
                            || ((debugWatchLine != environmentStack.peek().getCurrentLine())
                            && (debugActivationRecordSize == environmentStack.size())))
                    {
                        stepInSet = false;
                        askToBreak = true; //Ask the VM to break. but wait till we have processed all formals/functions
                    }

                }
                else if (stepOverSet)
                {
                    //Check to see if the line number has changed AND
                    //The activationRecord size has decreased (we've stepped out of the current function)
                    //OR (It has stayed the same, meaning the line number has just increased)
                    if ((debugWatchLine != environmentStack.peek().getCurrentLine())
                            && ((debugActivationRecordSize > environmentStack.size())
                            || (debugActivationRecordSize == environmentStack.size())))
                    {
                        stepOverSet = false;
                        askToBreak = true; //Ask the VM to break. but wait till we have processed all formals/functions
                    }
                }

                //Check to see if we are asking to break, make sure if we've hit a function
                //then we wait till we have processed formals
                if (askToBreak)
                {
                    //If we are asking to break but are inside a function call, wait till
                    //we have finished processing that function.
                    if (!this.debugJustEnteredFunction)
                    {
                        //Check to see if we have hit the function and that the next bytecode 
                        //is not a formal, if so we are able to break
                        if (!byteCodeIsFormal(currBC))
                        {
                            askToBreak = false;
                            isRunning = false;
                            break; //Break execution
                        }
                    }
                }

                //Execute the ByteCode
                pc++; //Increment program counter to our next instruction
                //this is done here so GotoCode, etc, can reset the program counter
                //and the VM will branch naturally
                currBC.execute(this);

                //Only resolve ByteCode if the vm is still running
                if (isRunning)
                {
                    currBC = program.requestByteCodeAtAddr(pc); //Get next ByteCode
                }
            }

        } catch (ByteCodeSyntaxErrorException ex)
        {
            System.out.println("Error running ByteCode, stopping in executeProgram()\n");
            System.exit(1);
        }

    }

    /**
     * <pre>Get the current execution line from the top of the environmentStack</pre>
     *
     * @return
     */
    public int getCurrentExecutionLine() {
        if (environmentStack.empty())
        {
            return 0;
        }
        else
        {
            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            return currEnvRecord.getCurrentLine();
        }
    }

    /**
     * Testing function to see if the enter bytecode is a formal
     *
     * @param code
     * @return
     */
    public boolean byteCodeIsFormal(ByteCode code) {
        if (code.getByteCodeName().equals("DebugFormalCode"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * <pre>Get the current function start line from the top of the environmentStack</pre>
     *
     * @return
     */
    public int getCurrentFunctionStartLine() {

        FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

        return currEnvRecord.getStartLine();

    }

    /**
     * <pre>Get the current function stop line from the top of the environmentStack</pre>
     *
     * @return
     */
    public int getCurrentFunctionStopLine() {

        FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

        return currEnvRecord.getStopLine();

    }

    /**
     * Enter a symbol at the current offset in the runStack
     *
     * @param sym
     */
    public void enterSymbolAtCurrentOffset(String sym) {
        if (!environmentStack.empty())
        {
            //Query runStack for current offset
            int runStackPtr = this.runStack.getCurrentStackPtr();

            //Adjust for starting at index 0
            runStackPtr--;

            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            currEnvRecord.enterSym(sym, runStackPtr);
        }
    }

    /**
     * Enter a symbol at the specified offset in the environment record
     *
     * @param sym
     * @param offset
     */
    public void enterSymbolAtOffset(String sym, int offset) {
        if (!environmentStack.empty())
        {

            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            currEnvRecord.enterSym(sym, offset);
        }
    }

    /**
     * Set a breakpoint at lineNum
     *
     * @param lineNum
     * @return
     */
    public boolean setBreakPoint(int lineNum) {
        if (validBreakpointLines.contains(lineNum))
        {
            //lineNum is valid to set BreakPoint
            sourceCodeLines.get(lineNum - 1).setBreak();
            return true;
        }
        else
        {
            return false; //Not valid line to break on, so send back false.
        }
    }

    /**
     * Unset a breakpoint at lineNum
     *
     * @param lineNum
     * @return
     */
    public boolean unsetBreakPoint(int lineNum) {
        if (validBreakpointLines.contains(lineNum))
        {
            //lineNum is valid to set BreakPoint
            sourceCodeLines.get(lineNum - 1).unsetBreak();
            return true;
        }
        else
        {
            return false; //Not valid line to break on, so send back false.
        }
    }

    /**
     * Unset all breakpoints
     */
    public void clearAllBreakpoints() {
        //Iterate over all breakpoints
        Iterator<Integer> breaks = validBreakpointLines.iterator();

        while (breaks.hasNext())
        {
            int i = breaks.next();
            //Make sure i is positive so we dont get Read/Write intrinsic functions
            if (i > 0)
            {
                //i is valid to unset BreakPoint
                sourceCodeLines.get(i - 1).unsetBreak();
            }
        }
    }

    /**
     * Tell the DebugVM to StepOut of the current executing function To do this
     * we set a flag, then record the size of the activationRecord If this size
     * becomes one less then we are outside the current function.
     */
    public void setStepOut() {
        stepOutSet = true;
        debugActivationRecordSize = environmentStack.size();
    }

    /**
     * Tell the DebugVM to StepOver the current executing line. To do this we
     * set the stepOverSet flag and disable breakpoints. We also save the size
     * of the activationRecord stack and the current line of execution. If
     * either changes then we break.
     */
    public void setStepOver() {

        stepOverSet = true;
        debugActivationRecordSize = environmentStack.size();
        debugWatchLine = environmentStack.peek().getCurrentLine();
    }

    /**
     * Tell the DebugVM to StepInto the function that the debugger is over. To
     * do this we set the StepInSet flag and disable breakpoints. We also save
     * the size of the activationRecord stack. If it grows then we break.
     */
    public void setStepIn() {
        stepInSet = true;
        debugActivationRecordSize = environmentStack.size();
        debugWatchLine = environmentStack.peek().getCurrentLine();
    }

    /**
     * Set function trace on/off
     *
     * @param flag
     */
    public void setTrace(boolean flag) {
        traceSet = flag;
    }

    /**
     * Return a list containing the line number of all currently set breakpoints
     *
     * @return
     */
    public List<Integer> getBreakPoints() {
        //Print all set breakpoints
        List<Integer> breakLines = new ArrayList();

        for (int i = 0; i < sourceCodeLines.size(); i++)
        {
            if (sourceCodeLines.get(i).isBreakptSet)
            {
                //+1 accounts for index starting at 0, sourcelines start at 1
                breakLines.add(i + 1);
            }
        }
        return breakLines;
    }

    /**
     * Retrieve a list of the current environment records
     *
     * @return
     */
    public List<FunctionEnvironmentRecord> getEnvironmentRecordList() {
        List<FunctionEnvironmentRecord> records = new ArrayList();

        for (FunctionEnvironmentRecord e : environmentStack)
        {
            records.add(e);
        }

        return records;
    }

    private void printTraceFunctionEntry(String funcName, int returnVal) {
        FunctionEnvironmentRecord currRecord;
        String output = "";
        int envStackSize = environmentStack.size();

        //Indent trace, no indenting for main line
        for (int i = 1; i < envStackSize-1; i++)
        {
            output += "  ";
        }

        if (!environmentStack.empty())
        {
            currRecord = environmentStack.peek();

            String[] splitFuncName = funcName.split("<"); //Use regexp to Remove <<2>> part of function
            String correctedFuncName = splitFuncName[0];  //The first entry of our regexp on something like
            //'fib<<2>>' will just be 'fib', if it doesn't have a
            //<<2>> component then it will just be 'Read' 

            //According to example Debugger the read function has no passed in value
            if (currRecord.getFunctionName().equals("Read"))
            {
                output += correctedFuncName + "()";
            }
            else
            {
                output += correctedFuncName + "(" + returnVal + ")";
            }
        }

        System.out.println(output);
    }

    /**
     * Print the function return value
     */
    public void printTraceFunctionReturn() {

        FunctionEnvironmentRecord currRecord;
        String funcName;
        int returnVal = 0;
        int envStackSize = environmentStack.size();

        if (this.traceSet)
        {
            String output = "";

            //Pretty indent for trace, no indention for main function
            for (int i = 1; i < envStackSize-1; i++)
            {
                output += "  ";
            }

            if (!environmentStack.empty())
            {
                currRecord = environmentStack.peek();
                funcName = currRecord.getFunctionName();

                if (!runStack.isEmpty())
                {
                    returnVal = runStack.peek();

                }
                output += "exit: " + funcName + ": " + returnVal;

                System.out.println(output);
            }

        }

    }

    /**
     * Retrieve the entire program's source code.
     *
     * @return
     */
    public Vector<Entry> getAllFunctionSource() {
        //Return the source code for the currently executing function
        //If there is no entry on environmentStack then we return the entire program

        Vector<Entry> currFunctionEntries;

        //No record started so return the entire program
        currFunctionEntries = sourceCodeLines;

        //Return the reference to our sourceCode
        return currFunctionEntries;

    }
}
