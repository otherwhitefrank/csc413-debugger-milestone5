package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ReturnCode;
import interpreter.debugger.DebugVirtualMachine;

/**
 * DebugReturnCode extends ReturnCode to also remove the current environment
 * record
 *
 * @author frank
 */
public class DebugReturnCode extends ReturnCode {

    public DebugReturnCode() {
        this.byteCodeName = "DebugReturnCode";
    }

    @Override
    public void execute(VirtualMachine vm) {
        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            
            //Before we pop tell DebugVM to print return value
            debugVM.printTraceFunctionReturn();
            
            //Call underlying return bytecode
            super.execute(vm);
            
            //Finally pop the EnvironmentRecord
            debugVM.popEnvironmentRecord();

            

        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugReturn ");
            System.exit(1);
        }
    }
}
